#!perl

use Test::More tests => 1;
use Data::Dumper;
use Geo::USCensus::TIGERweb;

diag( "Testing lookup of a census block" );
my $result = Geo::USCensus::TIGERweb->census_block_at_point(
  lat => 38.578793,
  lon => -121.48778,
  debug => 1,
);

ok( $result, 'Found a census block');
diag($result);

