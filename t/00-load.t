#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Geo::USCensus::TIGERweb' ) || print "Bail out!\n";
}

diag( "Testing Geo::USCensus::TIGERweb $Geo::USCensus::TIGERweb, Perl $], $^X" );
