package Geo::USCensus::TIGERweb::Service;

use strict;
use warnings;
use base 'Geo::USCensus::TIGERweb';
use Geo::USCensus::TIGERweb::Layer;

use List::Util qw(first);

sub layer {
  my $self = shift;
  my $id_or_name = shift;
  my $def = first { $_->{id} eq $id_or_name or $_->{name} eq $id_or_name }
            @{ $self->info->{layers} };
  if (!$def) {
    $self->{error} = "TIGERweb layer name/id '$id_or_name' not found";
    return;
  }
  return $self->create('Layer', $def->{id});
}

1;
