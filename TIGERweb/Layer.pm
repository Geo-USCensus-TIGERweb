package Geo::USCensus::TIGERweb::Layer;

use strict;
use warnings;
use base 'Geo::USCensus::TIGERweb';

my %geometryTypeGuess = (
  'x'       => 'esriGeometryPoint',
  'points'  => 'esriGeometryMultiPoint',
  'paths'   => 'esriGeometryPolyLine',
  'rings'   => 'esriGeometryPolygon',
);

sub query {
  my ($self, %param) = @_;
  my $g = $param{geometry} or die 'query: geometry required';
  if (!$param{geometryType}) {
    foreach (keys %geometryTypeGuess) {
      if (exists $g->{$_}) {
        $param{geometryType} = $geometryTypeGuess{$_};
        last;
      }
    }
  }
  my $fields = delete $param{fields};
  die 'query: fields required' if !$fields;
  $param{outFields} = $fields;
  # set a spatial reference in a sensible way
  my $wkid = $param{inSR} ||= '4326';
  $param{outSR} ||= $wkid;
  $g->{wkid} ||= $wkid;
  # default to find features that intersect
  $param{'spatialRel'} ||= 'esriSpatialRelIntersects';

  return $self->request('query', \%param);
}

1;
